package id42

import (
	"fmt"
	"math/rand"
	"time"
)

func randomEntry(s []string) string {
	rand.Seed(time.Now().UTC().UnixNano())
	r := rand.Intn(len(s) - 1)
	return s[r]
}

// Generate generates a random ID from an adjective a noun and a number below 100
func Generate() string {
	noun := randomEntry(nouns)
	adjective := randomEntry(adjectives)
	num := rand.Intn(99)
	return fmt.Sprintf("%s-%s-%d", adjective, noun, num)
}
